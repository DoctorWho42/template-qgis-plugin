# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## {version_tag} - YYYY-DD-mm

-->

## 3.22.10 - 2022-08-25

- Fix tests calling Qt widgets within QGIS Docker image
- Improve settings management using DataClass instead of NamedTuple
- Fix QTribu reference in GitHub config - Thanks @ismailsunni - See !12
- Remove QTribu reference in settings ui file

## 3.22.3 - 2022-05-12

- Improve GitLab release asset management

## 3.22.2 - 2022-05-12

- Update to QGIS 3.22 as minimum version
- Add pytest-qgis and flake8-qgis as development dependencies for the generated plugin
- Use Cookiecutter 1.7.3 to generate the plugin
- Add configuration section for pre-commit.ci if the selected CI/CD tool is GitHub
- Improve post hook
- Add a test to generate a demo plugin

## 0.1.0 - 2021-05-19

- First version, really minimalist
