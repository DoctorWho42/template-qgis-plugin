stages:
  - 🤞 test
  - 🚀 deploy

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_FOLDER/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  key:
    files:
      - requirements/*.txt
  paths:
    - .cache/pip

# -- TEST JOBS --------------------------------------------------------------------------
test-template-generation:
  stage: 🤞 test
  image: python:3.9
  only:
    - master
    - tags
  before_script:
    - apt install git
    - python3 -m pip install -U "cookiecutter>=1.7.0"
  script:
    # run templater
    - cookiecutter --no-input .
      author_name="QGIS Plugin Templater"
      ci_cd_tool=GitLab
      debug=yes
      plugin_category=Vector
      plugin_name="Plugin Templater Test"
      plugin_tags="test,ci,demonstration,template"
      post_git_init=yes
      post_install_venv=no
      repository_url_base="https://gitlab.com/Oslandia/qgis/template-qgis-plugin/"
    # zip the generated plugin to show
    - python3 -m zipfile -c generated_plugin.zip plugin_plugin_templater_test/

    # move to generated folder, then create and enable virtualenv
    - cd plugin_plugin_templater_test
    - python3 -m venv .venv
    - source .venv/bin/activate
    - python -m pip install -U pip setuptools wheel

    # install generated plugin requirements
    - python3 -m pip install -U -r requirements/development.txt
    - python3 -m pip install -U -r requirements/packaging.txt

    # linting and formatting code
    - black .
    - isort .
    - flake8 plugin_templater_test --count --select=E9,F63,F7,F82 --show-source --statistics
    - flake8 plugin_templater_test --count --exit-zero --max-complexity=10 --max-line-length=127 --statistics

    # upgrade
    - pre-commit autoupdate

    # faking it log to be able to run qgis-plugin-ci
    - git config --global user.email "qgis@oslandia.com"
    - git config --global user.name "Oslandia"
    - git add --all
    - git commit -am "Initial commit by QGIS Plugin Templater"
    - qgis-plugin-ci package latest --plugin-repo-url https://oslandia.gitlab.io/qgis/template-qgis-plugin/
  artifacts:
    name: "$PROJECT_FOLDER_b$CI_COMMIT_REF_NAME-c$CI_COMMIT_SHORT_SHA-j$CI_JOB_ID"
    paths:
      - generated_plugin.zip
      - "plugin_plugin_templater_test/plugin_templater_test.*.zip"
      - plugin_plugin_templater_test/plugins.xml
    expire_in: never

# -- DEPLOYMENT JOBS -------------------------------------------------------------------
pages:
  image: python:3.9-slim-buster
  stage: 🚀 deploy
  only:
    - master
  needs:
    - test-template-generation
  before_script:
    - python -m pip install -U -r requirements/base.txt
    - python -m pip install -U -r requirements/documentation.txt
  script:
    - ls
    - mkdir -p public
    # copy generated folder
    - cp generated_plugin.zip public/

    #  copy packaged plugin to public folder
    - cp plugin_plugin_templater_test/plugin_templater_test.*.zip public/
    - cp plugin_plugin_templater_test/plugins.xml public/
    - sphinx-build -b html docs public
  artifacts:
    paths:
      - public
    when: always

release-from-tag:
  stage: 🚀 deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: test-template-generation
      artifacts: true
  only:
    - tags
  script:
    - echo "Creating release from $CI_COMMIT_TAG with artifacts from job ${ARTIFACT_JOB_ID}."
    # - release-cli create --name "Version $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG
  release:
    name: "$CI_COMMIT_TAG"
    description: "Version $CI_COMMIT_TAG\n\n> Created using the GitLab release-cli during the job $CI_JOB_ID."
    tag_name: $CI_COMMIT_TAG
    assets:
      links:
        - name: "Generated plugin from the template"
          url: "https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/jobs/${CI_COMMIT_TAG}/artifacts/download?job=test-template-generation"
