# What you get

## Try it

Every commit on master branch triggers the templater to generate a plugin project, package and publish it, [using these parameters](https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/blob/master/.gitlab-ci.yml#L33-43).

### Download the generated plugin repository

Download the [zipped plugin project folder to inspect the structure](https://oslandia.gitlab.io/qgis/template-qgis-plugin/generated_plugin.zip).

### Test the generated and packaged plugin right into QGIS

You can add this URL into the QGIS extensions manager as plugin repository URL and look for `Plugin Templater Test` in the list of plugins:

```url
https://oslandia.gitlab.io/qgis/template-qgis-plugin/plugins.xml
```

## Code structure

> TO DOC

## Integrated settings

> TO DOC

## Help menu

> TO DOC
